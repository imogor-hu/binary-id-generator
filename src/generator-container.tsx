import { useState } from "react";
import { BinaryId, createBinaryId } from "@litbase/binary-id";
import { Panel } from "./panel.js";
import { toHex } from "./to-hex.js";

export function GeneratorContainer() {
  const [binaryId, setBinaryId] = useState(() => createBinaryId());

  return (
    <Panel>
      <h1>BinaryId Generator</h1>
      <hr />
      <button onClick={() => setBinaryId(createBinaryId())}>Refresh</button>
      <table>
        <tr>
          <td>Base64:</td>
          <td>
            <code>{binaryId.toBase64String()}</code>
          </td>
        </tr>
        <tr>
          <td>Url Safe String:</td>
          <td>
            <code>{binaryId.toUrlSafeString()}</code>
          </td>
        </tr>
        <tr>
          <td>MongoDB:</td>
          <td>
            <code>BinData(128,&quot;{binaryId.toBase64String()}&quot;)</code>
          </td>
        </tr>
        <tr>
          <td>HEX:</td>
          <td>
            <code>{getBinaryIdAsHex(binaryId)}</code>
          </td>
        </tr>
      </table>
      <hr />
      <footer>Click on the code to copy it to your clipboard</footer>
    </Panel>
  );
}

function getBinaryIdAsHex(id: BinaryId) {
  return toHex(id.toBinary());
}
