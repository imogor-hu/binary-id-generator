import { Panel } from "./panel";
import { useState } from "react";
import { BinaryId, INVALID_ID } from "@litbase/binary-id";
import { toHex } from "./to-hex.js";
import styled from "styled-components";

export function ParserContainer() {
  const [inputValue, setInputValue] = useState("");
  const parsedBinaryId = tryParseBinaryId(inputValue);

  return (
    <Panel>
      <h1>BinaryId Parser</h1>
      <hr />
      <StyledInput
        placeholder="Enter your BinaryId"
        type="text"
        onChange={(event) => setInputValue(event.currentTarget.value)}
        value={inputValue}
      />
      <table>
        <tr>
          <td>Is valid:</td>
          <td>
            <code>{parsedBinaryId.isInvalid ? "No" : "Yes"}</code>
          </td>
        </tr>
        <tr>
          <td>Date:</td>
          <td>
            <code>{parsedBinaryId.isInvalid ? "N/A" : parsedBinaryId.getDate().toISOString()}</code>
          </td>
        </tr>
        <tr>
          <td>Counter:</td>
          <td>
            <code>
              {parsedBinaryId.isInvalid
                ? "N/A"
                : getUint24(new DataView(parsedBinaryId.toBinary().buffer, 0, parsedBinaryId.length), 6)}
            </code>
          </td>
        </tr>
        <tr>
          <td>Random part:</td>
          <td>
            <code>{parsedBinaryId.isInvalid ? "N/A" : toHex(parsedBinaryId.toBinary().slice(7))}</code>
          </td>
        </tr>
        <tr>
          <td>Is nested:</td>
          <td>
            <code>{parsedBinaryId.isNested ? "Yes" : "No"}</code>
          </td>
        </tr>
      </table>
    </Panel>
  );
}

function tryParseBinaryId(value: string) {
  value = value.trim();

  if (!value) {
    return INVALID_ID;
  }

  try {
    const parsedId = BinaryId.fromUrlSafeString(value);

    if (!parsedId.isInvalid) {
      return parsedId;
    }
  } catch (error) { /* empty */ }

  try {
    const parsedId = BinaryId.fromBase64String(value);

    if (!parsedId.isInvalid) {
      return parsedId;
    }
  } catch (error) { /* empty */ }

  const match = value.match(/^BinData\(128,\s*"([^"]+)"\)$/);

  if (match) {
    try {
      const parsedId = BinaryId.fromBase64String(match[1]);

      if (!parsedId.isInvalid) {
        return parsedId;
      }
    } catch (error) { /* empty */ }
  }

  return INVALID_ID;
}

function getUint24(dataView: DataView, position: number) {
  let number = dataView.getUint8(position + 2);
  number += dataView.getUint16(position) << 8;

  return number;
}

const StyledInput = styled.input`
  margin: 0 auto;
  display: block;
`;
