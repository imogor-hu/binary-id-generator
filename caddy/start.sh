#!/usr/bin/env sh

/etc/caddy/env-replacer '/srv/**/*'
caddy run --config /etc/caddy/Caddyfile --adapter caddyfile
