FROM node:18 AS build

ARG BUILD_DATE=nodate
ENV BUILD_DATE=$BUILD_DATE

WORKDIR /src

COPY . /src/
RUN yarn install
RUN yarn build

FROM caddy:latest

COPY packages/web/caddy /etc/caddy/
ADD https://f003.backblazeb2.com/file/litbase-tools/env-replacer-alpine /etc/caddy/env-replacer
RUN chmod 755 /etc/caddy/env-replacer

COPY --from=build /src/dist /srv/

ENTRYPOINT /etc/caddy/start.sh
