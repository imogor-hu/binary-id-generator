import styled from 'styled-components'

export const Panel = styled.div`
  background-color: #20234d;
  color: white;
  padding: 25px;
  border-radius: 7px;

  & + & {
    margin-top: 16px;
  }

  button {
    margin: 8px auto;
    display: block;
  }
`
