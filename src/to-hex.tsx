export function toHex(value: Uint8Array) {
  return [...value].map((x) => x.toString(16).padStart(2, "0")).join("");
}
