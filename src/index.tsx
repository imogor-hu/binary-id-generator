import { createRoot } from 'react-dom/client'
import { App } from './app.js'

createRoot(document.getElementById('root') as HTMLElement).render(<App />)
