import { createGlobalStyle } from "styled-components";
import { GeneratorContainer } from "./generator-container.js";
import { ParserContainer } from "./parser-container.js";
import {MouseEvent} from "react";
import {Toaster, toast} from "react-hot-toast";

export function App() {
  return (
    <>
      <GlobalStyle />
      <div onClick={onWrapperClick}>
        <GeneratorContainer />
        <ParserContainer />
      </div>
      <Toaster/>
    </>
  );
}

async function onWrapperClick(event: MouseEvent<HTMLDivElement>) {
  const target = event.target as HTMLElement;

  if (target.tagName !== "CODE")
    return;

  try {
    await navigator.clipboard.writeText(String(target.textContent));

    toast.success("Copied to clipboard!");
  }
  catch (error) {
    toast.error(String(error));
  }
}

const GlobalStyle = createGlobalStyle`
  body {
    font-family: sans-serif;
    min-height: 100vh;
    display: flex;
    align-items: center;
    justify-content: center;
    background-color: #040719;
  }

  input {
    background-color: #0e1241;
    color: white;
    padding: 5px 8px;
    border-radius: 4px;
    border: 1px solid #21276e;
    
    &::placeholder {
      color: white;
    }
  }

  h1 {
    line-height: 1;
    font-weight: normal;
    text-align: center;
  }

  table td:first-child {
    text-align: right;
    padding-right: 10px;
    font-size: 1.1em;
  }

  td {
    line-height: 1.5;
    padding-top: 8px;
    padding-bottom: 8px;
    vertical-align: middle;
  }

  code {
    background-color: #0e1241;
    padding: 5px 8px;
    border-radius: 4px;
    cursor: pointer;
    transition: background-color ease-in-out 0.25s;
  }

  code:hover {
    background-color: #262b73;
  }

  footer {
    text-align: center;
    opacity: 0.9;
    font-size: 0.8em;
  }

  hr {
    appearance: none;
    width: 30px;
    height: 1px;
    background-color: white;
    border: none;
    margin: 15px auto;
    opacity: 0.8;
  }
`;
